using UnityEngine;

namespace CameraMove.Interfaces
{
    public abstract class CameraMovement : MonoBehaviour
    {
        public abstract void ResetCameraPos();
    }
}