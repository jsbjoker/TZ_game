using CameraMove.Interfaces;
using Inputs.Interfaces;
using UnityEngine;

namespace CameraMove.Controllers
{
    public class CameraMovementImp : CameraMovement
    {
        [SerializeField] private Transform _cameraTransform;
        [SerializeField] private PlayerInput _playerInput;
        [SerializeField] private Vector2 _xBorders;
        [SerializeField] private Vector2 _yBorders;
        private Vector3 _cameraOriginPosition;

        private void Awake()
        {
            _cameraOriginPosition = _cameraTransform.position;
            _playerInput.OnCameraMove += OnCameraMove;
        }

        public override void ResetCameraPos()
        {
            _cameraTransform.position = _cameraOriginPosition;
        }

        private void OnCameraMove(Vector3 position)
        {
            position *= -1f;
            var targetPosition = Vector3.Lerp(_cameraTransform.position, _cameraTransform.position + position,
                Time.deltaTime);
            targetPosition.x = Mathf.Clamp(targetPosition.x, _xBorders.x, _xBorders.y);
            targetPosition.y = Mathf.Clamp(targetPosition.y, _yBorders.x, _yBorders.y);
            _cameraTransform.position = targetPosition;
        }

        private void OnDestroy()
        {
            _playerInput.OnCameraMove -= OnCameraMove;
        }
    }
}