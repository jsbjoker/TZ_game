using Sounds.Enums;
using UnityEngine;

namespace Sounds.Interfaces
{
    public abstract class SoundStorage : MonoBehaviour
    {
        public abstract AudioClip GetClip(ESoundType soundType);
    }
}