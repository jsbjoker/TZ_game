using Sounds.Enums;
using UnityEngine;

namespace Sounds.Interfaces
{
    public abstract class SoundPlayer : MonoBehaviour
    {
        public abstract void Play(ESoundType soundType);
        public abstract void Play(ESoundType soundType, float volume);
    }
}