using Sounds.Enums;
using Sounds.Interfaces;
using UnityEngine;

namespace Sounds.Controllers
{
    public class SoundPlayerImp : SoundPlayer
    {
        [SerializeField] private SoundStorage _soundStorage;
        [SerializeField] private AudioSource _audioSource;
        
        
        public override void Play(ESoundType soundType)
        {
            Play(soundType, 1f);
        }

        public override void Play(ESoundType soundType, float volume)
        {
            _audioSource.volume = volume;
            _audioSource.PlayOneShot(_soundStorage.GetClip(soundType));
        }
    }
}