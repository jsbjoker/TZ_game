using System;
using System.Collections.Generic;
using System.Linq;
using Sounds.Data;
using Sounds.Enums;
using Sounds.Interfaces;
using UnityEngine;

namespace Sounds.Controllers
{
    public class SoundStorageImp : SoundStorage
    {
        [SerializeField] private List<AudioData> _audios;
        private Dictionary<ESoundType, AudioClip> _clips;

        private void Awake()
        {
            _clips = _audios.ToDictionary(k => k.SoundType, v => v.AudioClip);
        }

        public override AudioClip GetClip(ESoundType soundType) => _clips.ContainsKey(soundType)
            ? _clips[soundType]
            : throw new NullReferenceException($"{soundType} not found");
    }
}