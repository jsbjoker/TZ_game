using System;
using Sounds.Enums;
using UnityEngine;

namespace Sounds.Data
{
    [Serializable]
    public class AudioData
    {
        [SerializeField] private AudioClip _audioClip;
        [SerializeField] private ESoundType _soundType;

        public AudioClip AudioClip => _audioClip;
        public ESoundType SoundType => _soundType;
    }
}