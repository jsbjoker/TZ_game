using System;
using UnityEngine;

namespace Print.Interfaces
{
    public interface IPrint : IDisposable
    {
        event Action<IPrint> OnPrintPainted;
        string DefaulColorInHex { get; }
        bool CanChangeColor { get; }
        void ChangeColor(string color);
        Transform PrintTransform { get; }
        bool Painted { get; }
        void EnableHelp();
        void DisableHelp();
        void ResetPrint();
        void SetEmptyColor();
    }
}