using System;
using UnityEngine;

namespace Print.Interfaces
{
    public abstract class PrintCleaner : MonoBehaviour
    {
        public abstract void ClearColorsAfterDelay();
        public abstract event Action OnPrintCleared;
    }
}