using System.Collections.Generic;
using UnityEngine;

namespace Print.Interfaces
{
    public abstract class PrintStorage : MonoBehaviour
    {
        public abstract IReadOnlyList<IPrint> Prints { get; }
        public abstract IPrint GetPrintByTransform(Transform transform);
        public abstract void AddPrint(IPrint print);
        public abstract void RemovePrints();
    }
}