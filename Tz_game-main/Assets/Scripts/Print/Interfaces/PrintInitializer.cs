using System;
using UnityEngine;

namespace Print.Interfaces
{
    public abstract class PrintInitializer : MonoBehaviour
    {
        public abstract void Initialize();
    }
}