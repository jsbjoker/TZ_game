using Print.Views;
using UnityEngine;

namespace Print.Interfaces
{
    public abstract class PrintFactory : MonoBehaviour
    {
        public abstract void CreatePrint(PrintView printView);
    }
}