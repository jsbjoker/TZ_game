using UnityEngine;

namespace Print.Interfaces
{
    public interface IPrintView
    {
        Color PrintColor { get; }
        Color DefaultColor { get; }
        void ChangeMaterial(Material material);
        Material PrintMaterial { get; }
        Transform Transform { get; }
    }
}