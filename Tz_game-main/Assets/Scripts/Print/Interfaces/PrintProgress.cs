using UnityEngine;

namespace Print.Interfaces
{
    public abstract class PrintProgress : MonoBehaviour
    {
        public abstract void Initialize();
        public abstract void ClearProgress();
    }
}