using System.Collections.Generic;
using Print.Interfaces;
using Print.Views;
using UnityEngine;

namespace Print.Controllers
{
    public class PrintInitializerImp : PrintInitializer
    {
        [SerializeField] private PrintFactory _printFactory;
        [SerializeField] private List<PrintView> _prints;
        private PrintStorage gg;

        public override void Initialize()
        {
            foreach (var printView in _prints)
            {
                _printFactory.CreatePrint(printView);
            }
        }
    }
}