using System;
using System.Collections;
using Print.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Print.Controllers
{
    public class PrintHelpImp : PrintHelp
    {
        [SerializeField] private Button _helpButton;
        [SerializeField] private PrintStorage _printStorage;
        [SerializeField] private PrintCleaner _printCleaner;
        private readonly WaitForSeconds _helpDuration = new (3f);

        private void Awake()
        {
            _helpButton.interactable = false;
            _printCleaner.OnPrintCleared += EnableHelpButton;
            _helpButton.onClick.AddListener(ShowPrintHelp);
        }

        private void ShowPrintHelp()
        {
            StartCoroutine(ShowHelp());
        }

        private IEnumerator ShowHelp()
        {
            foreach (var print in _printStorage.Prints)
            {
                if (print.Painted) continue;
                print.EnableHelp();
            }

            yield return _helpDuration;
            
            foreach (var print in _printStorage.Prints)
            {
                if (print.Painted) continue;
                print.DisableHelp();
            }
        }

        private void EnableHelpButton()
        {
            _helpButton.interactable = true;
        }

        private void OnDestroy()
        {
            _helpButton.onClick.RemoveListener(ShowPrintHelp);
            _printCleaner.OnPrintCleared += EnableHelpButton;
        }
    }
}