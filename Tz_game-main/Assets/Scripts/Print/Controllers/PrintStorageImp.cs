using System;
using System.Collections.Generic;
using Print.Interfaces;
using UnityEngine;

namespace Print.Controllers
{
    public class PrintStorageImp : PrintStorage
    {
        private readonly List<IPrint> _prints = new ();
        
        public override IReadOnlyList<IPrint> Prints => _prints;

        public override IPrint GetPrintByTransform(Transform transform)
        {
            foreach (var print in _prints)
                if (print.PrintTransform == transform) return print;
            
            throw new NullReferenceException($"Print not found for object: {transform.name}");
        }

        public override void AddPrint(IPrint print)
        {
            _prints.Add(print);
        }

        public override void RemovePrints()
        {
            foreach (var print in _prints)
                print.Dispose();
            _prints.Clear();
        }

        private void OnDestroy()
        {
            foreach (var print in _prints)
                print.Dispose();
        }
    }
}