using System;
using Print.Interfaces;
using Sounds.Enums;
using Sounds.Interfaces;
using UnityEngine;

namespace Print.Controllers
{
    public class Print : IPrint
    {
        private readonly IPrintView _printView;
        private readonly SoundPlayer _soundPlayer;
        private string _lastColor;
        public event Action<IPrint> OnPrintPainted;
        private bool _tipped;
        
        public Transform PrintTransform => _printView.Transform;
        public bool Painted { get; private set; }
        public string DefaulColorInHex { get; }
        public string PrintColorInHex => ColorUtility.ToHtmlStringRGBA(_printView.PrintColor);
        
        public bool CanChangeColor => DefaulColorInHex != PrintColorInHex && !_tipped;

        public Print(IPrintView printView, SoundPlayer soundPlayer)
        {
            DefaulColorInHex = ColorUtility.ToHtmlStringRGBA(printView.DefaultColor);
            
            _printView = printView;
            _soundPlayer = soundPlayer;
        }
        
        public void ResetPrint()
        {
            if (!ColorUtility.TryParseHtmlString("#" + DefaulColorInHex, out var color)) return;
            SetColor(color);
            _lastColor = string.Empty;
            _tipped = false;
            Painted = false;
        }
        
        public void ChangeColor(string colorHex)
        {
            if (!ColorUtility.TryParseHtmlString("#" + colorHex, out var color)) return;
            SetColor(color);
            if (colorHex != DefaulColorInHex)
            {
                _soundPlayer.Play(ESoundType.Wrong);
                return;
            }
            _soundPlayer.Play(ESoundType.Correct);
            OnPrintPainted?.Invoke(this);
            Painted = true;
        }
        
        public void EnableHelp()
        {
            if (!ColorUtility.TryParseHtmlString("#" + DefaulColorInHex, out var color)) return;
            _lastColor = PrintColorInHex;
            SetColor(color);
            _tipped = true;
        }

        public void DisableHelp()
        {
            if (!ColorUtility.TryParseHtmlString("#" + _lastColor, out var color)) return;
            SetColor(color);
            _tipped = false;
            _lastColor = string.Empty;
        }

        

        public void SetEmptyColor()
        {
            SetColor(Color.white);
        }

        private void SetColor(Color color)
        {
            var newMaterial = new Material(_printView.PrintMaterial)
            {
                color = color
            };
            _printView.ChangeMaterial(newMaterial);
        }

        public void Dispose()
        {
            OnPrintPainted = null;
        }
    }
}