using System;
using System.Collections;
using Print.Interfaces;
using UnityEngine;

namespace Print.Controllers
{
    public class PrintCleanerImp : PrintCleaner
    {
        [SerializeField] private PrintStorage _printStorage;
        private readonly WaitForSeconds _delay = new (3f);
        private Coroutine _activeDelay;
        
        public override event Action OnPrintCleared;
        
        public override void ClearColorsAfterDelay()
        {
            if (_activeDelay != null)
                StopCoroutine(_activeDelay);
            
            _activeDelay = StartCoroutine(DelayBeforeClear());
        }

        private IEnumerator DelayBeforeClear()
        {
            yield return _delay;
            ClearPrints();
            _activeDelay = null;
        }

        private void ClearPrints()
        {
            foreach (var print in _printStorage.Prints)
                print.SetEmptyColor();
            OnPrintCleared?.Invoke();
        }

        private void OnDestroy()
        {
            OnPrintCleared = null;
        }
    }
}