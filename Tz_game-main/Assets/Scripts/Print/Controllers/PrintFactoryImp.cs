using Print.Interfaces;
using Print.Views;
using Sounds.Interfaces;
using UnityEngine;

namespace Print.Controllers
{
    public class PrintFactoryImp : PrintFactory
    {
        [SerializeField] private PrintStorage _printStorage;
        [SerializeField] private SoundPlayer _soundPlayer;
        
        public override void CreatePrint(PrintView printView)
        {
            var print = new Print(printView, _soundPlayer);
            _printStorage.AddPrint(print);
        }
    }
}