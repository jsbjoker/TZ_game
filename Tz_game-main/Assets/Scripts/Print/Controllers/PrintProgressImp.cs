using Print.Interfaces;
using Print.Views;
using UnityEngine;

namespace Print.Controllers
{
    public class PrintProgressImp : PrintProgress
    {
        [SerializeField] private PrintProgressView _printProgressView;
        [SerializeField] private PrintStorage _printStorage;
        private int _paintedPrintsAmount;


        public override void Initialize()
        {
            ClearProgress();
            foreach (var print in _printStorage.Prints)
            {
                print.OnPrintPainted += OnPrintPainted;
            }
        }

        public override void ClearProgress()
        {
            _paintedPrintsAmount = 0;
            _printProgressView.SetProgress(0f);
        }


        private void OnPrintPainted(IPrint print)
        {
            _paintedPrintsAmount++;
            _printProgressView.SetProgress((float)_paintedPrintsAmount / _printStorage.Prints.Count);
        }

        private void OnDestroy()
        {
            foreach (var print in _printStorage.Prints)
            {
                print.OnPrintPainted -= OnPrintPainted;
            }
        }
    }
}