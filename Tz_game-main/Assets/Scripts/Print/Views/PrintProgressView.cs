using UnityEngine;
using UnityEngine.UI;

namespace Print.Views
{
    public class PrintProgressView : MonoBehaviour
    {
        [SerializeField] private Image _progressBar;

        public void SetProgress(float progress)
        {
            _progressBar.fillAmount = progress;
        }
    }
}