using Print.Interfaces;
using UnityEngine;

namespace Print.Views
{
    public class PrintView : MonoBehaviour, IPrintView
    {
        [SerializeField] private MeshRenderer _printMesh;
        public Color PrintColor => _printMesh.material.color;
        public Color DefaultColor { get; private set; }

        private void Awake()
        {
            DefaultColor = _printMesh.material.color;
        }

        public void ChangeMaterial(Material material)
        {
            _printMesh.material = material;
        }

        public Material PrintMaterial => _printMesh.material;
        public Transform Transform => transform;
    }
}