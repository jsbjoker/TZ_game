using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Inputs.Views
{
    public class PlayerInputView : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private Vector3 _startDragPosition = Vector3.zero;
        
        public event Action<Vector3> OnRaycastClick;
        public event Action<Vector3> OnCameraMove;
        
        
        public void OnPointerClick(PointerEventData eventData)
        {
            if (_startDragPosition != Vector3.zero) return;
            OnRaycastClick?.Invoke(Input.mousePosition);
        }

        private void OnDestroy()
        {
            OnRaycastClick = null;
            OnCameraMove = null;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _startDragPosition = eventData.position;
        }

        public void OnDrag(PointerEventData eventData)
        {
            OnCameraMove?.Invoke(eventData.delta);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _startDragPosition = Vector3.zero;
        }
    }
}