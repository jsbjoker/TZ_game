using System;
using UnityEngine;

namespace Inputs.Interfaces
{ 
    public abstract class PlayerInput : MonoBehaviour
    {
        public abstract event Action<Vector3> OnRaycastClick;
        public abstract event Action<Vector3> OnCameraMove;
    }
}