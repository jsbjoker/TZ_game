using System;
using Inputs.Interfaces;
using Inputs.Views;
using UnityEngine;

namespace Inputs.Controllers
{
    public class PlayerInputImp : PlayerInput
    {
        [SerializeField] private PlayerInputView _inputView;
        public override event Action<Vector3> OnRaycastClick;
        public override event Action<Vector3> OnCameraMove;

        private void Awake()
        {
            _inputView.OnRaycastClick += HandleRaycastClick;
            _inputView.OnCameraMove += HandleCameraMove;
        }

        private void HandleRaycastClick(Vector3 position)
        {
            OnRaycastClick?.Invoke(position);
        }
        
        private void HandleCameraMove(Vector3 position)
        {
            OnCameraMove?.Invoke(position);
        }

        private void OnDestroy()
        {
            _inputView.OnRaycastClick -= HandleRaycastClick;
            _inputView.OnCameraMove -= HandleCameraMove;
            OnRaycastClick = null;
            OnCameraMove = null;
        }
    }
}