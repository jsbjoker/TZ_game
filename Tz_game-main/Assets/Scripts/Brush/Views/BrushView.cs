using Brush.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace Brush.Views
{
    public class BrushView : MonoBehaviour, IBrushView
    {
        [SerializeField] private Image _image;
        [SerializeField] private RectTransform _rect;

        public Color BrushColor => _image.color;

        public void SetBrushColor(Color color)
        {
            _image.color = color;
        }

        public RectTransform RectTransform => _rect;
        public void SetLocalScale(Vector3 scale) => _rect.localScale = scale;
        
        public void DestroyView()
        {
            Destroy(gameObject);
        }
    }
}