using UnityEngine;

namespace Brush.Interfaces
{
    public abstract class BrushInitializer : MonoBehaviour
    {
        public abstract void Initialize();
    }
}