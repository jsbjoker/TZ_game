using UnityEngine;

namespace Brush.Interfaces
{
    public interface IBrush
    {
        string ColorInHex { get; }
        RectTransform BrushRect { get; }
        void SetLocalScale(Vector3 scale);
        void RemoveBrush();
    }
}