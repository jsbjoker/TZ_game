using UnityEngine;

namespace Brush.Interfaces
{
    public abstract class BrushFactory : MonoBehaviour
    {
        public abstract void CreateBrush(string colorInHex);
    }
}