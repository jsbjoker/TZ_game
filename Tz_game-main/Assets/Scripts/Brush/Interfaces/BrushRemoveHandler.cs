using Scroll.Interfaces;
using UnityEngine;

namespace Brush.Interfaces
{
    public abstract class BrushRemoveHandler : MonoBehaviour
    {
        public abstract void Initialize(IScrollPresenter scrollPresenter);
    }
}