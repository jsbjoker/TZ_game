using UnityEngine;

namespace Brush.Interfaces
{
    public interface IBrushView
    {
        Color BrushColor { get; }
        void SetBrushColor(Color color);
        RectTransform RectTransform { get; }
        void SetLocalScale(Vector3 scale);
        void DestroyView();
    }
}