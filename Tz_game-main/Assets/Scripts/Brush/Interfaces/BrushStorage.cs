using System.Collections.Generic;
using UnityEngine;

namespace Brush.Interfaces
{
    public abstract class BrushStorage : MonoBehaviour
    {
        public abstract IReadOnlyDictionary<string, IBrush> Brushes { get; }
        public abstract IBrush GetBrushByColor(string color);
        public abstract bool ContainBrushWithColor(string color);
        public abstract void AddBrush(IBrush brush);
        public abstract void RemoveBrush(IBrush brush);
    }
}