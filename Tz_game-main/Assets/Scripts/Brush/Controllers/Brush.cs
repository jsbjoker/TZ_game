using Brush.Interfaces;
using UnityEngine;

namespace Brush.Controllers
{
    public class Brush : IBrush
    {
        private readonly IBrushView _brushView;
        public string ColorInHex => ColorUtility.ToHtmlStringRGBA(_brushView.BrushColor);
        public RectTransform BrushRect => _brushView.RectTransform;
        public void SetLocalScale(Vector3 scale) => _brushView.SetLocalScale(scale);

        public Brush(IBrushView brushView)
        {
            _brushView = brushView;
        }
        
        public void RemoveBrush()
        {
            _brushView.DestroyView();
        }
    }
}