using System;
using System.Collections.Generic;
using Brush.Interfaces;

namespace Brush.Controllers
{
    public class BrushStorageImp : BrushStorage
    {
        private readonly Dictionary<string, IBrush> _brushes = new ();

        public override IReadOnlyDictionary<string, IBrush> Brushes => _brushes;

        public override IBrush GetBrushByColor(string color) => _brushes.ContainsKey(color)
            ? _brushes[color]
            : throw new NullReferenceException($"Brush with color: {color} not found");

        public override bool ContainBrushWithColor(string color) => _brushes.ContainsKey(color);

        public override void AddBrush(IBrush brush)
        {
            if (_brushes.ContainsKey(brush.ColorInHex)) return;
            _brushes.Add(brush.ColorInHex, brush);
        }

        public override void RemoveBrush(IBrush brush)
        {
            if (!_brushes.ContainsKey(brush.ColorInHex)) return;
            var removeBrush = _brushes[brush.ColorInHex];
            removeBrush.RemoveBrush();
            _brushes.Remove(removeBrush.ColorInHex);
        }
    }
}