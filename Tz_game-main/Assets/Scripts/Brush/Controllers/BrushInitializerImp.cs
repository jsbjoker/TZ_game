using Brush.Interfaces;
using Print.Interfaces;
using UnityEngine;

namespace Brush.Controllers
{
    public class BrushInitializerImp : BrushInitializer
    {
        [SerializeField] private BrushFactory _brushFactory;
        [SerializeField] private PrintStorage _printStorage;


        public override void Initialize()
        {
            foreach (var print in _printStorage.Prints)
            {
                _brushFactory.CreateBrush(print.DefaulColorInHex);
            }
        }
    }
}