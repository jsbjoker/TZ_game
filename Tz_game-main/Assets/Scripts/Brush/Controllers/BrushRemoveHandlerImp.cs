using Brush.Interfaces;
using Print.Interfaces;
using Scroll.Interfaces;
using UnityEngine;

namespace Brush.Controllers
{
    public class BrushRemoveHandlerImp : BrushRemoveHandler
    {
        [SerializeField] private PrintStorage _printStorage;
        [SerializeField] private BrushStorage _brushStorage;
        private IScrollPresenter _scrollPresenter;

        public override void Initialize(IScrollPresenter scrollPresenter)
        {
            _scrollPresenter = scrollPresenter;
            foreach (var print in _printStorage.Prints)
            {
                print.OnPrintPainted += OnPrintPainted;
            }
        }

        private void OnPrintPainted(IPrint paintedPrint)
        {
            var containtPrintWithSameColor = false;
            foreach (var print in _printStorage.Prints)
            {
                if(print == paintedPrint) continue;
                if (print.DefaulColorInHex == paintedPrint.DefaulColorInHex && !print.Painted)
                {
                    containtPrintWithSameColor = true;
                    break;
                }
            }
            if (containtPrintWithSameColor) return;
            var brush = _brushStorage.GetBrushByColor(paintedPrint.DefaulColorInHex);
            _brushStorage.RemoveBrush(brush);
            _scrollPresenter.ScrollToClosiesBrush();
        }

        private void OnDestroy()
        {
            foreach (var print in _printStorage.Prints)
            {
                print.OnPrintPainted -= OnPrintPainted;
            }
        }
    }
}