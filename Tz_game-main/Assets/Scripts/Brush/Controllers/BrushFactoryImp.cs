using Brush.Interfaces;
using Brush.Views;
using UnityEngine;

namespace Brush.Controllers
{
    public class BrushFactoryImp : BrushFactory
    {
        [SerializeField] private BrushStorage _brushStorage;
        [SerializeField] private BrushView _brushView;
        [SerializeField] private RectTransform _scrollRect;
        
        public override void CreateBrush(string colorInHex)
        {
            var hexToConver = "#" + colorInHex;
            if (_brushStorage.ContainBrushWithColor(colorInHex)) return;
            if (!ColorUtility.TryParseHtmlString(hexToConver, out var color)) return;
            var brushView = Instantiate(_brushView, _scrollRect);
            brushView.gameObject.name = "Brush: " + colorInHex;
            brushView.SetBrushColor(color);
            var brush = new Brush(brushView);
            _brushStorage.AddBrush(brush);
            
        }
    }
}