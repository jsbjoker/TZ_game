using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameState.Views
{
    public class EndGamePanelView : MonoBehaviour
    {
        [SerializeField] private Button _restartButton;

        public event Action OnRestartClick;
        
        private void Awake()
        {
            _restartButton.onClick.AddListener(() =>
            {
                OnRestartClick?.Invoke();
            });
        }

        public void SetPanelActiveState(bool value) => gameObject.SetActive(value);

        private void OnDestroy()
        {
            OnRestartClick = null;
            _restartButton.onClick.RemoveAllListeners();
        }
    }
}