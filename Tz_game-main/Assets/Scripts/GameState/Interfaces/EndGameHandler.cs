using Scroll.Interfaces;
using UnityEngine;

namespace GameState.Interfaces
{
    public abstract class EndGameHandler : MonoBehaviour
    {
        public abstract void Initialize(IScrollPresenter scrollPresenter);
    }
}