using Brush.Interfaces;
using GameState.Interfaces;
using Paint.Interfaces;
using Print.Interfaces;
using Scroll.Interfaces;
using UnityEngine;

namespace GameState.Controllers
{
    public class GameInitializer : MonoBehaviour
    {
        [SerializeField] private PrintInitializer _printInitializer;
        [SerializeField] private BrushInitializer _brushInitializer;
        [SerializeField] private ScrollInitializer _scrollInitializer;
        [SerializeField] private PaintInitializer _paintInitializer;
        [SerializeField] private PrintCleaner _printCleaner;
        [SerializeField] private BrushRemoveHandler _brushRemoveHandler;
        [SerializeField] private PrintProgress _printProgress;
        [SerializeField] private EndGameHandler _endGameHandler;

        private void Start()
        {
            _printInitializer.Initialize();
            _brushInitializer.Initialize();
            var paintProcess = _paintInitializer.Initialize();
            var scrollPresenter = _scrollInitializer.Initialize(paintProcess);
            _printProgress.Initialize();
            _endGameHandler.Initialize(scrollPresenter);
            _brushRemoveHandler.Initialize(scrollPresenter);
            _printCleaner.ClearColorsAfterDelay();
        }

        public void RestartGame()
        {
            
            
            
        }
    }
}