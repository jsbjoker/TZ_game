using Brush.Interfaces;
using CameraMove.Interfaces;
using GameState.Interfaces;
using GameState.Views;
using Print.Interfaces;
using Scroll.Interfaces;
using UnityEngine;

namespace GameState.Controllers
{
    public class EndGameHandlerImp : EndGameHandler
    {
        [SerializeField] private PrintStorage _printStorage;
        [SerializeField] private EndGamePanelView _endGamePanelView;
        [SerializeField] private GameInitializer _gameInitializer;
        [SerializeField] private CameraMovement _cameraMovement;
        [SerializeField] private BrushInitializer _brushInitializer;
        [SerializeField] private PrintCleaner _printCleaner;
        [SerializeField] private PrintProgress _printProgress;
        private int _paintedPrints;
        private IScrollPresenter _scrollPresenter;

        private void Awake()
        {
            _endGamePanelView.OnRestartClick += RestartGame;
        }

        private void RestartGame()
        {
            foreach (var print in _printStorage.Prints)
                print.ResetPrint();
            
            _brushInitializer.Initialize();
            _cameraMovement.ResetCameraPos();
            _gameInitializer.RestartGame();
            _scrollPresenter.Initialize();
            _paintedPrints = 0;
            _printProgress.ClearProgress();
            _printCleaner.ClearColorsAfterDelay();
            _endGamePanelView.SetPanelActiveState(false);
        }

        public override void Initialize(IScrollPresenter scrollPresenter)
        {
            _scrollPresenter = scrollPresenter;
            foreach (var print in _printStorage.Prints)
            {
                print.OnPrintPainted += OnPrintPainted;
            }
        }

        private void OnPrintPainted(IPrint print)
        {
            _paintedPrints++;
            if (_paintedPrints < _printStorage.Prints.Count) return;
            _endGamePanelView.SetPanelActiveState(true);
        }

        private void OnDestroy()
        {
            _endGamePanelView.OnRestartClick -= RestartGame;
        }
    }
}