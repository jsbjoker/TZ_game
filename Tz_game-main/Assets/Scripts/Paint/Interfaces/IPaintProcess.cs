using Brush.Interfaces;

namespace Paint.Interfaces
{
    public interface IPaintProcess
    {
        void SetBrush(IBrush brush);
    }
}