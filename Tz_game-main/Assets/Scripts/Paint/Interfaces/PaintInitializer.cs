using UnityEngine;

namespace Paint.Interfaces
{
    public abstract class PaintInitializer : MonoBehaviour
    {
        public abstract IPaintProcess Initialize();
    }
}