using Inputs.Interfaces;
using Paint.Interfaces;
using Print.Interfaces;
using UnityEngine;

namespace Paint.Controllers
{
    public class PaintInitializerImp : PaintInitializer
    {
        [SerializeField] private LayerMask _printMask;
        [SerializeField] private PrintStorage _printStorage;
        [SerializeField] private PlayerInput _playerInput;
        private PaintProcess _paintProcess;

        public override IPaintProcess Initialize()
        {
            _paintProcess = new PaintProcess(_printMask, _playerInput, _printStorage);
            return _paintProcess;
        }

        private void OnDestroy()
        {
            _paintProcess.Dispose();
        }
    }
}