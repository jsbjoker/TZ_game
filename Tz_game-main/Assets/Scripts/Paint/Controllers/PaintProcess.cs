using Brush.Interfaces;
using Inputs.Interfaces;
using Paint.Interfaces;
using Print.Interfaces;
using UnityEngine;

namespace Paint.Controllers
{
    public class PaintProcess : IPaintProcess
    {
        private readonly LayerMask _printMask;
        private readonly PlayerInput _playerInput;
        private readonly PrintStorage _printStorage;
        private IBrush _selectedBrush;

        public PaintProcess(LayerMask printMask, PlayerInput playerInput,
            PrintStorage printStorage)
        {
            _printMask = printMask;
            _playerInput = playerInput;
            _printStorage = printStorage;
            _playerInput.OnRaycastClick += TryPaintPrint;
        }
        
        public void SetBrush(IBrush brush)
        {
            _selectedBrush = brush;
        }
        
        private void TryPaintPrint(Vector3 startPosition)
        {
            if (_selectedBrush == null) return;
            if (!Physics.Raycast(Camera.main.ScreenPointToRay(startPosition), out var hit, 10, _printMask)) return;
            var print = _printStorage.GetPrintByTransform(hit.transform);
            if (!print.CanChangeColor) return;
            print.ChangeColor(_selectedBrush.ColorInHex);
        }


        public void Dispose()
        {
            _playerInput.OnRaycastClick -= TryPaintPrint;
        }
    }
}