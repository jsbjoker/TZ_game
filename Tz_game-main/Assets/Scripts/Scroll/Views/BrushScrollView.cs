using Scroll.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Scroll.Views
{
    public class BrushScrollView : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private IScrollPresenter _scrollPresenter;
        
        public void Initialize(IScrollPresenter scrollPresenter)
        {
            _scrollPresenter = scrollPresenter;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            
        }

        public void OnDrag(PointerEventData eventData)
        {
            _scrollPresenter?.OnDrag();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _scrollPresenter?.OnEndDrag();
        }
    }
}