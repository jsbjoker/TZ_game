using Brush.Interfaces;
using Paint.Interfaces;
using Scroll.Interfaces;
using Scroll.Settings;
using Scroll.Views;
using UnityEngine;
using UnityEngine.UI;

namespace Scroll.Controllers
{
    public class ScrollInitializerImp : ScrollInitializer
    {
        [SerializeField] private ScrollRect _brushScrollRect;
        [SerializeField] private ScrollSettings _scrollSettings;
        [SerializeField] private BrushScrollView _brushScrollView;
        [SerializeField] private BrushStorage _brushStorage;


        public override IScrollPresenter Initialize(IPaintProcess paintProcess)
        {
            var scrollPresenter = new ScrollPresenter(_scrollSettings,
                _brushScrollRect, _brushStorage, this, paintProcess);
            _brushScrollView.Initialize(scrollPresenter);
            scrollPresenter.Initialize();
            return scrollPresenter;
        }
    }
}