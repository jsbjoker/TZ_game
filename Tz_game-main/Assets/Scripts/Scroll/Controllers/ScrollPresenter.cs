using System.Collections;
using Brush.Interfaces;
using Paint.Interfaces;
using Scroll.Interfaces;
using Scroll.Settings;
using UnityEngine;
using UnityEngine.UI;

namespace Scroll.Controllers
{
    public class ScrollPresenter : IScrollPresenter
    {
        private readonly ScrollSettings _scrollSettings;
        private readonly ScrollRect _brushScrollRect;
        private readonly BrushStorage _brushStorage;
        private readonly MonoBehaviour _coroutineManager;
        private readonly IPaintProcess _paintProcess;
        private Coroutine _activeCoroutine;
        private float _centerNormalizedPosition;

        public ScrollPresenter(ScrollSettings scrollSettings, ScrollRect brushScrollRect,
            BrushStorage brushStorage, MonoBehaviour coroutineManager, IPaintProcess paintProcess)
        {
            _scrollSettings = scrollSettings;
            _brushScrollRect = brushScrollRect;
            _brushStorage = brushStorage;
            _coroutineManager = coroutineManager;
            _paintProcess = paintProcess;
        }

        public void Initialize()
        {
            Canvas.ForceUpdateCanvases();
            _brushScrollRect.verticalNormalizedPosition = 0.5f;
            ScrollToClosiesBrush();
        }
        
        public void ScrollToClosiesBrush()
        {
            OnEndDrag();
        }

        public void OnBeginDrag()
        {
            if (_activeCoroutine == null) return;
            _coroutineManager.StopCoroutine(_activeCoroutine);
            _activeCoroutine = null;
        }

        public void OnDrag()
        {
            foreach (var brush in _brushStorage.Brushes.Values)
            {
                var clampedValue = GetClampedVerticalNormalizedPosition(brush);
                var resultSize = Mathf.Lerp(0f, 1f, clampedValue);
                brush.SetLocalScale(Vector3.Lerp(_scrollSettings.MaxScale, _scrollSettings.MinScale, resultSize * 2.5f));
            }
        }

        public void OnEndDrag()
        {
            var closiestBrush = FindNearestToCenterBrush();
            if (closiestBrush == null) return;
            _paintProcess.SetBrush(closiestBrush);
            _coroutineManager.StartCoroutine(SnapToBrushAfterUpdateFrame(closiestBrush));
        }

        private IEnumerator SnapToBrushAfterUpdateFrame(IBrush brush)
        {
            yield return new WaitForEndOfFrame();
            SnapToBrush(brush);
        }

        private IBrush FindNearestToCenterBrush()
        {
            IBrush closiestBrush = null;
            foreach (var brush in _brushStorage.Brushes.Values)
            {
                if (closiestBrush == null)
                {
                    closiestBrush = brush;
                    continue;
                }
                var previousPosition = GetClampedVerticalNormalizedPosition(closiestBrush);
                var currentPosition = GetClampedVerticalNormalizedPosition(brush);
                if (currentPosition < previousPosition)
                {
                    closiestBrush = brush;
                }
            }

            return closiestBrush;
        }

        private void SnapToBrush(IBrush brush)
        {
            Vector2 itemCenterPoint = _brushScrollRect.content
                .InverseTransformPoint( brush.BrushRect.transform.TransformPoint(brush.BrushRect.rect.center));

            var contentSizeOffset = _brushScrollRect.content.rect.size;
            contentSizeOffset.Scale(_brushScrollRect.content.pivot);
            
            var contentSize = _brushScrollRect.content.rect.size;
            var viewportSize = ((RectTransform)_brushScrollRect.content.parent).rect.size;
            var contentScale = _brushScrollRect.content.localScale;
            var focusPoint = itemCenterPoint + contentSizeOffset;
            contentSize.Scale(contentScale);
            focusPoint.Scale(contentScale);

            var scrollPosition = _brushScrollRect.verticalNormalizedPosition;
            if( _brushScrollRect.vertical && contentSize.y > viewportSize.y )
                scrollPosition = Mathf.Clamp01((focusPoint.y - viewportSize.y * 0.5f) / (contentSize.y - viewportSize.y));
            _activeCoroutine = _coroutineManager.StartCoroutine(SmoothMovingToPosition(scrollPosition));
        }

        private IEnumerator SmoothMovingToPosition(float targetValue)
        {
            var startTime = Time.time;
            
            while (Time.time - startTime <= 1f / _scrollSettings.SnapScrollSpeed)
            {
                var elapsedTime = Time.time - startTime;
                _brushScrollRect.verticalNormalizedPosition = Mathf.Lerp(_brushScrollRect.verticalNormalizedPosition,
                    targetValue, elapsedTime * _scrollSettings.SnapScrollSpeed);
                OnDrag();
                yield return null;
            }
            _brushScrollRect.verticalNormalizedPosition = targetValue;
            OnDrag();
        }

        private float GetClampedVerticalNormalizedPosition(IBrush brush)
        {
            return Mathf.Clamp01(Mathf.Abs(0.5f - GetVerticalNormalizedPos(_brushScrollRect.viewport, brush.BrushRect)));
        }
        
        private float GetVerticalNormalizedPos(RectTransform targetRect, RectTransform target)
        {
            return Rect.PointToNormalized(targetRect.rect, GetLocalPosInRect(target.position, targetRect)).y;
        }

        private Vector2 GetLocalPosInRect(Vector3 position, RectTransform targetRect)
        {
            var point = RectTransformUtility.WorldToScreenPoint(null, position);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(targetRect, point, null, out var localPos);
            return localPos;
        }
    }
}