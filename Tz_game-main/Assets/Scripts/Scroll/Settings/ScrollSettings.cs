using UnityEngine;

namespace Scroll.Settings
{
    [CreateAssetMenu(fileName = "ScrollSettings", menuName = "Settings/Game/ScrollSettings")]
    public class ScrollSettings : ScriptableObject
    {
        [SerializeField] private Vector3 _minScale;
        [SerializeField] private Vector3 _maxScale;
        [SerializeField] private float _snapScrollSpeed;
        public Vector3 MinScale => _minScale;
        public Vector3 MaxScale => _maxScale;
        public float SnapScrollSpeed => _snapScrollSpeed;
    }
}