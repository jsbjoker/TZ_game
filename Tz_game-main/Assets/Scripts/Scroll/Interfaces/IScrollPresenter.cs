namespace Scroll.Interfaces
{
    public interface IScrollPresenter
    {
        void OnBeginDrag();
        void OnDrag();
        void OnEndDrag();
        void ScrollToClosiesBrush();
        void Initialize();
    }
}