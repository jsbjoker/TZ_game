using Paint.Interfaces;
using UnityEngine;

namespace Scroll.Interfaces
{
    public abstract class ScrollInitializer : MonoBehaviour
    {
        public abstract IScrollPresenter Initialize(IPaintProcess paintProcess);
    }
}